#!/usr/bin/ruby

require 'tty-prompt'
require 'yaml'

Dir.glob("functions/*.rb").each do |functions|
  require "./#{functions}"
end

prompt = TTY::Prompt.new

preset = prompt.select("Welches Preset soll installiert werden?", per_page: 8, filter: true) do |menu|
  menu.default 2

  menu.choice 'grafik'
  menu.choice 'office'
  menu.choice 'lager'
  menu.choice 'plain'
  menu.choice ' ', 5, disabled: ' '
  menu.choice 'dev'
  menu.choice 'custom'
  menu.choice 'update [Install package updates]'
  menu.choice 'Exit'
end

if(preset.downcase =~ /update*/)
  shellexec("brew update && brew upgrade && brew cask upgrade", "Das Update ist fehlgeschlagen!")

  puts "Die Aktualisierung der installierten Packete wurde abgeschlossen!"
  exit
elsif(preset.downcase =~ /custom*/)
  puts "Custom"

  custom = prompt.ask("Welche Pakete sollen installiert werden? (Schema: packet1, (cask) paket2):") do |q|
    q.required true
    q.modify   :down
  end

  custom = custom.split(", ")

  for package in custom do
    if package.include? "(cask)"
      package.delete "(cask) "

      shellexec("brew cask install #{package}", "Der Cask #{package} konnte nicht installiert werden!")
    else
      shellexec("brew install #{package}", "Das Packet #{package} konnte nicht installiert werden!")
    end
  end
elsif(preset.downcase =~ /exit/)
	abort("Aaand.... It's gone!")
else
  installPackages(preset)
end

select_lang = setLang(prompt)

short_comp = setCompanies(prompt)

pc_name = setHostname(prompt,short_comp)

setDefaults()

serial = `system_profiler SPHardwareDataType | awk '/Serial/ {print $4}'`

puts "Eingestellte Sprache: #{select_lang}"
puts "Firma: #{company}"
puts "Hostname: #{pc_name}"
puts "Seriennummer: #{serial}"
