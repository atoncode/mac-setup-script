# MacOS Auto-Setup

Ein Script, um die Standard-Konfiguration von Mac-Computern interaktiv vorzunehmen.

Das Script beinhaltet eigene Konfigurationsdateien, welche festlegen, welche Pakete installiert werden (Auch brew taps können hinzugefügt werden).<br />
Es werden automatisch Zuordnungen an ein Departement einer Firma festgelegt und daraus wird der Hostname festgelegt und die Seriennummer wird ausgelesen.<br />
Zu guter letzt wird das Dock und einige andere systemeinstellungen so konfiguriert, dass es den Standards entspricht.

<br />

**Zur Installation kann das folgende Script im Terminal ausgeführt werden:**
``` bash
curl https://gitlab.com/fadendaten/mac-setup-script/raw/master/setup.sh | sh
```

<br />

**Die Konfigurationsmöglichkeiten im Detail:**

1. Paketinstallation

<table>
	<tr>
 		<th>
   			Menüpunkt
 		</th>
 		<th>
  			Pakete
 		</th>
	</tr>
   <tr>
	    <td>
	       	Grafik
	    </td>
	    <td>
			- Dockutil <br />
	    	- iTerm2 <br />
			- Dropbox <br />
			- Microsoft Office (Word, Excel, PowerPoint, Outlook, OneNote) <br />
			- Skype for Business <br />
			- Google Chrome <br />
			- TeamViewer (Host only) <br />
			- Adobe Creative Cloud
	    </td>
   	</tr>
	<tr>
 	    <td>
 	       	Office
 	    </td>
 	    <td>
 			- Dockutil <br />
 	    	- iTerm2 <br />
 			- Dropbox <br />
 			- Microsoft Office (Word, Excel, PowerPoint, Outlook, OneNote) <br />
 			- Skype for Business <br />
 			- Google Chrome <br />
 			- TeamViewer (Host only) <br />
 	    </td>
    </tr>
	<tr>
 	    <td>
 	       	Lager
 	    </td>
 	    <td>
 			- Dockutil <br />
 	    	- iTerm2 <br />
 			- Microsoft Office (Word, Excel, PowerPoint, Outlook, OneNote) <br />
 			- Google Chrome <br />
 			- TeamViewer (Host only) <br />
 	    </td>
    </tr>
	<tr>
 	    <td>
 	       	Plain
 	    </td>
 	    <td>
 			- Dockutil <br />
 	    	- iTerm2 <br />
 			- Google Chrome <br />
 			- TeamViewer (Host only) <br />
 	    </td>
    </tr>
	<tr>
 	    <td>
 	       	Dev
 	    </td>
 	    <td>
 			- Dockutil <br />
			- Aareguru (Als Test)
 	    	- iTerm2 <br />
			- Dropbox <br />
			- Microsoft Office (Word, Excel, PowerPoint, Outlook, OneNote) <br />
			- Skype for Business <br />
 			- Google Chrome <br />
 			- TeamViewer (Client) <br />
 	    </td>
    </tr>
</table>

2. Sprache
  * en-US
  * en-GB
  * de-CH
  * de-DE
  * de-AT


3. Computername

 <table>
 	<tr>
  		<th>
    		Firma
  		</th>
  		<th>
   			Kürzel
  		</th>
		<th>
    		Planet
  		</th>
  		<th>
   			Kürzel
  		</th>
		<th>
    		Land
  		</th>
  		<th>
   			Kürzel
  		</th>
 	</tr>
    <tr>
 	    <td>
 	       	Nile
 	    </td>
 	    <td>
 			nile
 	    </td>
		<td>
 	       	Erde
 	    </td>
 	    <td>
 			e
 	    </td>
		<td>
 	       	- Schweiz <br />
			- Deutschland <br />
			- Österreich <br />
 	    </td>
 	    <td>
 			- che <br />
			- deu <br />
			- aut <br />
 	    </td>
    </tr>
	<tr>
 	    <td>
 	       	Pluvina
 	    </td>
 	    <td>
 			pluv
 	    </td>
		<td>
 	       	Erde
 	    </td>
 	    <td>
 			e
 	    </td>
		<td>
 	       	- Schweiz <br />
 	    </td>
 	    <td>
 			- che <br />
 	    </td>
    </tr>
	<tr>
 	    <td>
 	       	ZAS
 	    </td>
 	    <td>
 			zas
 	    </td>
		<td>
 	       	Erde
 	    </td>
 	    <td>
 			e
 	    </td>
		<td>
 	       	- Schweiz <br />
 	    </td>
 	    <td>
 			- che <br />
 	    </td>
    </tr>
	<tr>
 	    <td>
 	       	Bijou les Boutiques
 	    </td>
 	    <td>
 			biju
 	    </td>
		<td>
 	       	Erde
 	    </td>
 	    <td>
 			e
 	    </td>
		<td>
 	       	- Schweiz <br />
 	    </td>
 	    <td>
 			- che <br />
 	    </td>
    </tr>
	<tr>
 	    <td>
 	       	Fadendaten
 	    </td>
 	    <td>
 			fade
 	    </td>
		<td>
 	       	Erde
 	    </td>
 	    <td>
 			e
 	    </td>
		<td>
 	       	- Schweiz <br />
 	    </td>
 	    <td>
 			- che <br />
 	    </td>
    </tr>
 </table>

Dazu kommt eine dreistellige Nummer, welche innerhalb der Firma eindeutig ist. <br />
**Bsp: fade-e-che-001**

 4. Paketupdate

Die Option "Update" aktualisiert alle installierten Brew-Pakete.


 5. Custom Setup

Die Option "Custom" ermöglicht es, dass ein selber definiertes Set an Paketen mittels Brew installiert werden kann. <br />
Die Pakete müssen im Schema "paket1, (cask) paket2" eingegeben werden.
