def setDefaults()
  defaults = YAML.load_file('./configs/defaults.yaml')

  for default in defaults do
    shellexec("#{default}","Die Konfiguration #{default} konnte nicht gesetzt werden!")
  end
end
