#!/bin/bash

# Ask for the administrator password upfront.
sudo -v

# Keep-alive: update existing `sudo` time stamp until `setup.sh` has finished
while true; do sudo -n true; sleep 60; kill -0 "$$" || exit; done 2>/dev/null &

sudo softwareupdate -a -i &

if [ -f "./setup.rb" ]; then
	ruby ./setup.rb
else
	if [ $1 == "dev" ]; then
		curl -o setup.zip https://gitlab.com/fadendaten/mac-setup-script/-/archive/dev/mac-setup-script-dev.zip
	else
	  	curl -o setup.zip https://gitlab.com/fadendaten/mac-setup-script/-/archive/master/mac-setup-script-master.zip
	fi

	unzip setup.zip -d setup

	cd setup/*

	sudo gem install bundle

	bundle

	# Check for Homebrew
	if test ! $(which brew); then
	  # Install Homewbrew
	  /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
	fi

	clear

	ruby ./setup.rb
fi
